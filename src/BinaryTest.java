import Genetics.Chromosomes.BinaryChromosome;
import Genetics.Chromosomes.EncodingType;
import Genetics.Evolution.Crossover.CrossoverType;
import Genetics.Evolution.FitnessFunction;
import Genetics.Evolution.Mutation.MutationType;
import Genetics.Evolution.Selection.SelectionType;
import Genetics.Generation;

public class BinaryTest {

    public static void main(String[] args) {
        FitnessFunction fitnessFunction = (genotype) -> {
            final float maxWeight = 15f;
            float actualWeight = 0f;
            float actualValue = 0f;

            for (int i = 0; i < genotype.length; i++) {
                if ((Boolean) genotype[i]) {
                    switch(i) {
                        case 0:
                            actualWeight += 10;
                            actualValue += 11;
                            break;

                        case 1:
                            actualWeight += 4;
                            actualValue += 8;
                            break;

                        case 2:
                            actualWeight += 5;
                            actualValue += 6;
                            break;

                        case 3:
                            actualWeight += 2;
                            actualValue += 12;
                            break;

                        case 4:
                            actualWeight += 8;
                            actualValue += 5;
                            break;

                        case 5:
                            actualWeight += 2;
                            actualValue += 8;
                            break;

                        case 6:
                            actualWeight += 6;
                            actualValue += 4;
                            break;

                        case 7:
                            actualWeight += 7;
                            actualValue += 13;
                            break;
                    }
                }
            }

            if (actualWeight > maxWeight) {
                return 0f;
            }

            return 1 - (1 / actualValue);
        };

        Generation generation = new Generation(EncodingType.Binary, fitnessFunction);
        generation.advance(SelectionType.Roulette, CrossoverType.TPX, MutationType.BitInversion, 100);

        BinaryChromosome solution = (BinaryChromosome) generation.getSolution();

        moreInformation(solution.getDna());
        System.out.println("Done");
    }

    static void moreInformation(Object[] genotype) {
        float acutalWeight = 0f;
        float actualValue = 0f;

        for (int i = 0; i < genotype.length; i++) {
            if ((Boolean) genotype[i]) {
                System.out.print("1");
                switch(i) {
                    case 0:
                        acutalWeight += 10;
                        actualValue += 11;
                        break;

                    case 1:
                        acutalWeight += 4;
                        actualValue += 8;
                        break;

                    case 2:
                        acutalWeight += 5;
                        actualValue += 6;
                        break;

                    case 3:
                        acutalWeight += 2;
                        actualValue += 12;
                        break;

                    case 4:
                        acutalWeight += 8;
                        actualValue += 5;
                        break;

                    case 5:
                        acutalWeight += 2;
                        actualValue += 8;
                        break;

                    case 6:
                        acutalWeight += 6;
                        actualValue += 4;
                        break;

                    case 7:
                        acutalWeight += 7;
                        actualValue += 13;
                        break;
                }
            } else {
                System.out.print("0");
            }
        }
        System.out.println();
        System.out.println("Weight: " + acutalWeight +  " Value: " + actualValue);
    }
}
