import Genetics.Chromosomes.EncodingType;
import Genetics.Chromosomes.ValueChromosome;
import Genetics.Evolution.Crossover.CrossoverType;
import Genetics.Evolution.FitnessFunction;
import Genetics.Evolution.Mutation.MutationType;
import Genetics.Evolution.Selection.SelectionType;
import Genetics.Generation;

public class ValueTest {

    public static void main(String[] args) {
        FitnessFunction fitnessFunction = (genotype) -> {
            Float[] dna = (Float[]) genotype;

            return 1 + 1/Math.abs(5 - (dna[0] + dna[1] + dna[2] + dna[3] + dna[4]));
        };

        for (int i = 0; i < 100; i++) {
            Generation generation = new Generation(EncodingType.Value, fitnessFunction);
            generation.advance(SelectionType.Roulette, CrossoverType.SPX, MutationType.RandomAddition, 10000);

            ValueChromosome solution = (ValueChromosome) generation.getSolution();
            System.out.println(solution);
            moreInformation(solution.getDna());
        }
        System.out.println("Done");
    }

    static void moreInformation(Object[] genotype) {
        float result = 0f;

        for (Object gene : genotype) {
            float value = (Float) gene;
            result += value;
        }

        System.out.println(result);
    }
}
