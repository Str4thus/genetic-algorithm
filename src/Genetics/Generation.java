package Genetics;

import Genetics.Chromosomes.*;
import Genetics.Evolution.Crossover.CrossoverType;
import Genetics.Evolution.FitnessFunction;
import Genetics.Evolution.Mutation.MutationHandler;
import Genetics.Evolution.Mutation.MutationType;
import Genetics.Evolution.Selection.SelectionHandler;
import Genetics.Evolution.Selection.SelectionType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class Generation<T extends Chromosome> {
    private T type;
    private List<T> currentPool = new ArrayList<>();
    private List<T> nextPool = new ArrayList<>();
    private List<T> bestSolutions = new ArrayList<>();

    public Generation(EncodingType encodingType, FitnessFunction fitnessFunction) {
        switch (encodingType) {
            case Binary:
                this.type = (T) new BinaryChromosome(fitnessFunction);
                break;

            case Permutation:
                this.type = (T) new PermutationChromosome(fitnessFunction);
                break;

            case Value:
                this.type = (T) new ValueChromosome(fitnessFunction);
                break;
        }

        initializePopulation();
    }


    public T getSolution() {
        Collections.sort(bestSolutions);

        return bestSolutions.get(0); // Overall best solution
    }

    public void advance(SelectionType selectionType, CrossoverType crossoverType, MutationType mutationType) {
        SelectionHandler<T> selectionHandler = new SelectionHandler<>();
        MutationHandler<T> mutationHandler = new MutationHandler<>();

        nextPool = new ArrayList<>();

        // Selection + Crossover
        switch (selectionType) {
            case Roulette:
                this.nextPool = selectionHandler.roulette(currentPool, crossoverType);
                break;

            case Rank:
                this.nextPool = selectionHandler.rank(currentPool, crossoverType);
                break;

            case Elitism:
                this.nextPool = selectionHandler.elitism(currentPool, crossoverType);
                break;
        }

        // Mutation
        for (T chromosome : nextPool) {
            int index = nextPool.indexOf(chromosome);

            switch (mutationType) {
                case BitInversion:
                    nextPool.set(index, mutationHandler.bitInversion(chromosome));
                    break;

                case OrderChanging:
                    nextPool.set(index, mutationHandler.orderChanging(chromosome));
                    break;

                case RandomAddition:
                    nextPool.set(index, mutationHandler.randomAddition(chromosome));
                    break;
            }
        }

        Collections.sort(nextPool);
        bestSolutions.add(nextPool.get(0)); // Add best chromosome from this generation

        this.currentPool = this.nextPool;
    }

    public void advance(SelectionType selectionType, CrossoverType crossoverType, MutationType mutationType, int times) {
        for (int i = 0; i < times; i++) {
            advance(selectionType, crossoverType, mutationType);
        }
    }

    private void initializePopulation() {
        for (int i = 0; i < Nature.POPULATION_SIZE; i++) {
            this.currentPool.add((T) type.createEmptyChromosome());
        }
    }
}
