package Genetics.Evolution;

public interface FitnessFunction {
    float fitness(Object[] dna);
}
