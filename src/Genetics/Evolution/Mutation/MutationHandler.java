package Genetics.Evolution.Mutation;

import Genetics.Chromosomes.Chromosome;
import Genetics.Nature;

import java.util.Random;

public class MutationHandler<T extends Chromosome> {
    private final Random r = new Random();

    public T bitInversion(T chromosome) {
        for (int i = 0; i < Nature.GENE_SIZE; i++) {
            chromosome.getDna()[i] = (r.nextFloat() < Nature.BIT_INVERSION_PROBABILITY)
                    ? !((Boolean) chromosome.getDna()[i]) : chromosome.getDna()[i];
        }

        return chromosome;
    }

    public T orderChanging(T chromosome) {
        for (int i = 0; i < Nature.GENE_SIZE; i++) {
            if (r.nextFloat() < Nature.ORDER_CHANGING_PROBABILITY) {
                int firstIndex = r.nextInt(Nature.GENE_SIZE);
                int secondIndex = r.nextInt(Nature.GENE_SIZE);

                Object valueOne = chromosome.getDna()[firstIndex];
                Object valueTwo = chromosome.getDna()[secondIndex];

                chromosome.getDna()[firstIndex] = valueTwo;
                chromosome.getDna()[secondIndex] = valueOne;
            }
        }

        return chromosome;
    }

    public T randomAddition(T chromosome) {
        try {
            for (int i = 0; i < chromosome.getDna().length; i++) {
                if (r.nextFloat() < Nature.RANDOM_ADDITION_PROBABILITY) {
                    int negate = (r.nextBoolean()) ? 1 : -1;

                    ((Float[]) chromosome.getDna())[i] += (r.nextFloat() * Nature.RANDOM_ADDITION_MAX_VALUE) * negate;
                }
            }
        } catch (Exception e) {
            System.out.println("Fix me :c");
        }

        return chromosome;
    }
}
