package Genetics.Evolution.Mutation;

public enum MutationType {
    BitInversion,
    OrderChanging,
    RandomAddition
}
