package Genetics.Evolution.Crossover;

import Genetics.Chromosomes.Chromosome;
import Genetics.Nature;

import java.util.Random;

public class CrossoverHandler<T extends Chromosome> {
    private final Random r = new Random();

    /*
        Permutation Single Point Crossover
     */
    public T pspx(T fitParent, T weakParent) {
        T child = (T) fitParent.createEmptyChromosome();

        for (int i = 0; i < Nature.GENE_SIZE; i++) {
            child.setGene(i, 0);
        }

        int crossoverPoint = r.nextInt(Nature.GENE_SIZE);
        int secondPartOffset = 0;

        for (int i = 0; i < Nature.GENE_SIZE; i++) {

            if (i < crossoverPoint)
                child.setGene(i, fitParent.getDna()[i]);
            else {

                for (int j = 0; j < weakParent.getDna().length; j++) {
                    boolean isPresent = false;
                    for (int k = 0; k < child.getDna().length; k++) {
                        if (weakParent.getDna()[j].equals(child.getDna()[k])) {
                            isPresent = true;
                        }
                    }

                    if (!isPresent) {
                        child.getDna()[crossoverPoint + secondPartOffset] = weakParent.getDna()[j];
                        secondPartOffset++;
                    }
                }
            }
        }

        return child;
    }

    /*
        Single Point Crossover
     */
    public T spx(T fitParent, T weakParent) {
        T child = (T) fitParent.createEmptyChromosome();

        int crossoverPoint = r.nextInt(Nature.GENE_SIZE);

        for (int i = 0; i < Nature.GENE_SIZE; i++) {
            child.getDna()[i] = (i < crossoverPoint) ? fitParent.getDna()[i] : weakParent.getDna()[i];
        }

        return child;
    }

    /*
        Two Point Crossover
     */
    public T tpx(T fitParent, T weakParent) {
        T child = (T) fitParent.createEmptyChromosome();

        int firstCrossoverPoint = r.nextInt(Nature.GENE_SIZE);
        int secondCrossoverPoint = r.nextInt(Nature.GENE_SIZE);

        for (int i = 0; i < Nature.GENE_SIZE; i++) {
            child.getDna()[i] = (i < firstCrossoverPoint || i > secondCrossoverPoint) ? fitParent.getDna()[i] : weakParent.getDna()[i];
        }

        return child;
    }

    /*
        Uniform Crossover
     */
    public T ux(T fitParent, T weakParent) {
        T child = (T) fitParent.createEmptyChromosome();

        for (int i = 0; i < Nature.GENE_SIZE; i++) {
            child.getDna()[i] = (r.nextFloat() < Nature.UNIFORM_CROSSOVER_PROBABILITY) ? fitParent.getDna()[i] : weakParent.getDna()[i];
        }

        return child;
    }
}
