package Genetics.Evolution.Crossover;

public enum CrossoverType {
    PSPX,
    SPX,
    TPX,
    UX
}
