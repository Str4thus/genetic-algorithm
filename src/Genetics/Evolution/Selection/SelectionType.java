package Genetics.Evolution.Selection;

public enum SelectionType {
    Roulette,
    Rank,
    Elitism
}
