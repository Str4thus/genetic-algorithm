package Genetics.Evolution.Selection;

import Genetics.Chromosomes.Chromosome;
import Genetics.Evolution.Crossover.CrossoverHandler;
import Genetics.Evolution.Crossover.CrossoverType;
import Genetics.Nature;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SelectionHandler<T extends Chromosome> {


    private final Random r = new Random();
    private CrossoverHandler<T> crossoverHandler = new CrossoverHandler<>();

    public List<T> roulette(List<T> currentPool, CrossoverType crossoverType) {
        List<T> nextPool = new ArrayList<>();

        float fitnessSum = 0f;

        for (Chromosome chromosome : currentPool) {
            fitnessSum += chromosome.fitness();
        }

        float fitnessCounter;
        float randomNumber;

        while (nextPool.size() != currentPool.size()) {
            T parent1 = null, parent2 = null;

            do {
                // Select parent 1
                if (parent1 == null) {
                    r.setSeed(System.nanoTime());
                    randomNumber = r.nextFloat();
                    for (T parent : currentPool) {
                        fitnessCounter = parent.fitness() / fitnessSum;
                        if (fitnessCounter < randomNumber)
                            parent1 = parent;
                    }
                }

                // Select parent 2
                if (parent2 == null) {
                    r.setSeed(System.nanoTime());
                    randomNumber = r.nextFloat();
                    for (T parent : currentPool) {
                        fitnessCounter = parent.fitness() / fitnessSum;
                        if (fitnessCounter < randomNumber && !parent.equals(parent1))
                            parent2 = parent;
                    }
                }
            } while (parent1 == null || parent2 == null);

            // Crossover
            nextPool.add(this.crossover(parent1, parent2, crossoverType));
        }

        return nextPool;
    }

    public List<T> rank(List<T> currentPool, CrossoverType crossoverType) {
        // TODO implement
        return null;
    }

    public List<T> elitism(List<T> currentPool, CrossoverType crossoverType) {
        List<T> nextPool = new ArrayList<>();
        int indexToCopyTo = (int) Math.floor(Nature.GENE_SIZE * Nature.ELITISM_PERCENTAGE);

        // Move best chromosomes into the next generation
        for (int i = 0; i <= indexToCopyTo; i++) {
            nextPool.add(currentPool.get(i));
        }


        // Regular crossover for the other places ( Selected by ROULETTE)
        float fitnessSum = 0f;

        List<T> remainingPool = currentPool.subList(indexToCopyTo+1, currentPool.size()-1);
        for (Chromosome chromosome : remainingPool) {
            fitnessSum += chromosome.fitness();
        }

        float fitnessCounter;
        float randomNumber;


        while (nextPool.size() != currentPool.size()) {
            T parent1 = null, parent2 = null;

            do {
                if (parent1 != null && parent1.equals(parent2))
                    parent2 = null;

                // Select parent 1
                if (parent1 == null) {
                    randomNumber = r.nextFloat();
                    for (T parent : remainingPool) {
                        fitnessCounter = parent.fitness() / fitnessSum;
                        if (fitnessCounter < randomNumber)
                            parent1 = parent;
                    }
                }

                // Select parent 2
                if (parent2 == null) {
                    randomNumber = r.nextFloat();
                    for (T parent : remainingPool) {
                        fitnessCounter = parent.fitness() / fitnessSum;
                        if (fitnessCounter < randomNumber && !parent.equals(parent1))
                            parent2 = parent;
                    }
                }

            } while (parent1 == null || parent2 == null);

            // Crossover
            nextPool.add(this.crossover(parent1, parent2, crossoverType));
        }

        return nextPool;
    }

    private T crossover(T parent1, T parent2, CrossoverType crossoverType) {
        switch (crossoverType) {
            case PSPX:
                return crossoverHandler.pspx(parent1, parent2);

            case SPX:
                return crossoverHandler.spx(parent1, parent2);

            case TPX:
                return crossoverHandler.tpx(parent1, parent2);

            case UX:
                return crossoverHandler.ux(parent1, parent2);
        }

        return null;
    }
}
