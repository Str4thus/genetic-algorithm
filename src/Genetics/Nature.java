package Genetics;

public final class Nature {
    public static final int GENE_SIZE = 5;
    public static final int POPULATION_SIZE = 100;
    public static final float UNIFORM_CROSSOVER_PROBABILITY = 0.6f;
    public static final float BIT_INVERSION_PROBABILITY = 0.1f;
    public static final float ORDER_CHANGING_PROBABILITY = 0.1f;
    public static final float RANDOM_ADDITION_PROBABILITY = 0.4f;
    public static final float ELITISM_PERCENTAGE = 0.1f;
    public static final float RANDOM_ADDITION_MAX_VALUE = 5f;

    public static final int VALUE_ENCODING_RANGE_MIN = -10;
    public static final int VALUE_ENCODING_RANGE_MAX = 10;
}
