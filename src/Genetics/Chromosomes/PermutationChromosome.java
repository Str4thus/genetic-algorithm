package Genetics.Chromosomes;

import Genetics.Evolution.FitnessFunction;
import Genetics.Nature;

import java.util.Arrays;
import java.util.Collections;

public class PermutationChromosome extends Chromosome<Integer, PermutationChromosome> {

    public PermutationChromosome(FitnessFunction fitnessFunction) {
        super(fitnessFunction);
        this.dna = new Integer[Nature.GENE_SIZE];
    }

    @Override
    public PermutationChromosome createEmptyChromosome() {
        PermutationChromosome chromosome = new PermutationChromosome(fitnessFunction);

        for (int i = 0; i < Nature.GENE_SIZE; i++)
            chromosome.dna[i] = i+1;
        Collections.shuffle(Arrays.asList(chromosome.dna));

        return chromosome;
    }

    @Override
    public Integer[] getDna() {
        return this.dna;
    }

    @Override
    public void setGene(int index, Integer value) {
        this.dna[index] = value;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (Integer gene : dna) {
            builder.append(gene).append(" ");
        }

        return builder.toString();
    }
}
