package Genetics.Chromosomes;

public enum EncodingType {
    Binary,
    Permutation,
    Value
}
