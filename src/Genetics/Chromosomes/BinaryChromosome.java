package Genetics.Chromosomes;

import Genetics.Evolution.FitnessFunction;
import Genetics.Nature;

import java.util.Random;

public class BinaryChromosome extends Chromosome<Boolean, BinaryChromosome> {

    public BinaryChromosome(FitnessFunction fitnessFunction) {
        super(fitnessFunction);
        this.dna = new Boolean[Nature.GENE_SIZE];
    }

    @Override
    public BinaryChromosome createEmptyChromosome() {
        Random r = new Random();
        BinaryChromosome chromosome = new BinaryChromosome(this.fitnessFunction);

        for (int i = 0; i < chromosome.getDna().length; i++)
            chromosome.setGene(i, r.nextBoolean());

        return chromosome;
    }

    @Override
    public Boolean[] getDna() {
        return this.dna;
    }

    @Override
    public void setGene(int index, Boolean value) {
        this.dna[index] = value;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (Boolean gene : dna) {
            if (gene)
                builder.append("1");
            else
                builder.append("0");
        }

        return builder.toString();
    }
}
