package Genetics.Chromosomes;

import Genetics.Evolution.FitnessFunction;
import Genetics.Nature;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class ValueChromosome extends Chromosome<Float, ValueChromosome> {

    public ValueChromosome(FitnessFunction fitnessFunction) {
        super(fitnessFunction);
        this.dna = new Float[Nature.GENE_SIZE];
    }

    @Override
    public ValueChromosome createEmptyChromosome() {
        Random r = new Random();
        ValueChromosome chromosome = new ValueChromosome(fitnessFunction);

        for (int i = 0; i < Nature.GENE_SIZE; i++)
            chromosome.dna[i] = ThreadLocalRandom.current().nextInt(Nature.VALUE_ENCODING_RANGE_MIN,
                    Nature.VALUE_ENCODING_RANGE_MAX+1) * r.nextFloat();

        return chromosome;
    }

    @Override
    public Float[] getDna() {
        return this.dna;
    }

    @Override
    public void setGene(int index, Float value) {
        this.dna[index] = value;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (Number value : dna) {
            builder.append(value).append(" ");
        }

        return builder.toString();
    }
}
