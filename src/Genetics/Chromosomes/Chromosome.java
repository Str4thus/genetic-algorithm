package Genetics.Chromosomes;

import Genetics.Evolution.FitnessFunction;

public abstract class Chromosome<T, Q extends Chromosome> implements Comparable<Chromosome> {
    protected FitnessFunction fitnessFunction;
    protected T[] dna;

    public Chromosome(FitnessFunction fitnessFunction) {
        this.fitnessFunction = fitnessFunction;
    }

    public abstract T[] getDna();
    public abstract Q createEmptyChromosome();
    public abstract void setGene(int index, T value);

    public float fitness() {
        return this.fitnessFunction.fitness(dna);
    }

    @Override
    public int compareTo(Chromosome other) {
        // Descending sort by fitness
        return Float.compare(other.fitness(), this.fitness());
    }
}
