import Genetics.Chromosomes.EncodingType;
import Genetics.Chromosomes.PermutationChromosome;
import Genetics.Evolution.Crossover.CrossoverType;
import Genetics.Evolution.FitnessFunction;
import Genetics.Evolution.Mutation.MutationType;
import Genetics.Evolution.Selection.SelectionType;
import Genetics.Generation;

import java.awt.*;
import java.util.HashMap;

public class PermutationTest {

    public static void main(String[] args) {
        FitnessFunction fitnessFunction = (dna) -> {
            HashMap<Object, Point> cities = new HashMap<>();
            cities.put(1, new Point(0, 0));
            cities.put(2, new Point(1, 1));
            cities.put(3, new Point(0, 1));
            cities.put(4, new Point(-1, 1));
            cities.put(5, new Point(-1, 0));
            cities.put(6, new Point(-1, -1));
            cities.put(7, new Point(1, -1));

            float totalDistance = 0f;

            for (int i = 1; i < dna.length; i++) {
                totalDistance += Math.sqrt(Math.pow(cities.get(dna[i - 1]).x - cities.get(dna[i]).x, 2)
                        + Math.pow(cities.get(dna[i - 1]).y - cities.get(dna[i]).y, 2));

            }

            totalDistance += Math.sqrt(Math.pow(cities.get(dna[dna.length-1]).x - cities.get(dna[0]).x, 2)
                    + Math.pow(cities.get(dna[dna.length-1]).y - cities.get(dna[0]).y, 2));

            return (1 / totalDistance);
        };

        Generation generation = new Generation(EncodingType.Permutation, fitnessFunction);
        generation.advance(SelectionType.Roulette, CrossoverType.PSPX, MutationType.OrderChanging, 100);

        PermutationChromosome solution = (PermutationChromosome) generation.getSolution();
        System.out.println(solution);

        System.out.println("Done");
    }
}
